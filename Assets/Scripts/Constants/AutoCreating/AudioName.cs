﻿/// <summary>
/// オーディオ名を定数で管理するクラス
/// </summary>
public static class AudioName
{
	  public const string KENKEN = "kenken";
	  public const string LEVEL1 = "level1";
	  public const string LEVEL2 = "level2";
	  public const string LEVEL3 = "level3";
	  public const string LEVEL4 = "level4";
}
