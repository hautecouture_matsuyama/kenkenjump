﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class Data : ScriptableObject
{
    public string[] texts;
}