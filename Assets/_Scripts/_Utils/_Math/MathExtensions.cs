﻿using UnityEngine;
using System.Collections;

public static class MathExtensions
{
	public static float map (float value, float istart, float istop, float ostart, float ostop) 
	{
		return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
	}

	public static float InverseAbs (float input)
	{
		return Mathf.Abs (input) * (-1);
	}

	public static int InverseAbs (int input)
	{
		return Mathf.Abs (input) * (-1);
	}
}
