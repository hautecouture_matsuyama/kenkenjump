﻿using UnityEngine;
using System.Collections;

public class CameraExtensions
{
	/// <summary>
	/// Calculates a bounding box that contains all the targets.
	/// </summary>
	public static Rect CalculateTargetsBoundingBox (params Vector3[] targets)
	{
		float minX = Mathf.Infinity;
		float maxX = Mathf.NegativeInfinity;
		float minY = Mathf.Infinity;
		float maxY = Mathf.NegativeInfinity;
		
		foreach (Vector3 target in targets) 
		{
			minX = Mathf.Min (minX, target.x);
			minY = Mathf.Min (minY, target.y);
			maxX = Mathf.Max (maxX, target.x);
			maxY = Mathf.Max (maxY, target.y);
		}
		
		return Rect.MinMaxRect (minX, maxY, maxX, minY);
	}

	public static Rect CalculateTargetsBoundingBox (params Vector2[] targets)
	{
		float minX = Mathf.Infinity;
		float maxX = Mathf.NegativeInfinity;
		float minY = Mathf.Infinity;
		float maxY = Mathf.NegativeInfinity;
		
		foreach (Vector3 target in targets) 
		{
			minX = Mathf.Min (minX, target.x);
			minY = Mathf.Min (minY, target.y);
			maxX = Mathf.Max (maxX, target.x);
			maxY = Mathf.Max (maxY, target.y);
		}
		
		return Rect.MinMaxRect (minX, maxY, maxX, minY);
	}

	public static Rect CalculateTargetsBoundingBox (float paddingLeft = 0.0f, float paddingRight = 0.0f, float paddingTop = 0.0f, float paddingBottom = 0.0f, params Vector3[] targets)
	{
		float minX = Mathf.Infinity;
		float maxX = Mathf.NegativeInfinity;
		float minY = Mathf.Infinity;
		float maxY = Mathf.NegativeInfinity;
		
		foreach (Vector3 target in targets) 
		{
			minX = Mathf.Min (minX, target.x);
			minY = Mathf.Min (minY, target.y);
			maxX = Mathf.Max (maxX, target.x);
			maxY = Mathf.Max (maxY, target.y);
		}
		
		return Rect.MinMaxRect (minX - paddingLeft, maxY + paddingTop, maxX + paddingRight, minY - paddingBottom);
	}

	public static Rect CalculateTargetsBoundingBox (float paddingLeft = 0.0f, float paddingRight = 0.0f, float paddingTop = 0.0f, float paddingBottom = 0.0f, params Vector2[] targets)
	{
		float minX = Mathf.Infinity;
		float maxX = Mathf.NegativeInfinity;
		float minY = Mathf.Infinity;
		float maxY = Mathf.NegativeInfinity;
		
		foreach (Vector3 target in targets) 
		{
			minX = Mathf.Min (minX, target.x);
			minY = Mathf.Min (minY, target.y);
			maxX = Mathf.Max (maxX, target.x);
			maxY = Mathf.Max (maxY, target.y);
		}
		
		return Rect.MinMaxRect (minX - paddingLeft, maxY + paddingTop, maxX + paddingRight, minY - paddingBottom);
	}

	/// <summary>
	/// Calculates a camera position given the a bounding box containing all the targets.
	/// </summary>
	public static Vector3 CalculateCameraPosition (Rect boundingBox, float CameraZPos)
	{
		Vector2 boundingBoxCenter = boundingBox.center;
		
		return new Vector3 (boundingBoxCenter.x, boundingBoxCenter.y, CameraZPos);
	}

	public static Vector3 CalculateCameraPosition (Rect boundingBox, float CameraZPos, float xOffset)
	{
		Vector2 boundingBoxCenter = boundingBox.center;
		
		return new Vector3 (boundingBoxCenter.x - xOffset, boundingBoxCenter.y, CameraZPos);
	}

	/// <summary>
	/// Calculates a new ortho
    /// size for the camera based on the target bounding box.
	/// </summary>
	public static float CalculateOrthographicSize (Rect boundingBox, Camera camera, float minSize, float zoomSpeed)
	{
		float orthographicSize = camera.orthographicSize;
		Vector3 topRight = new Vector3 (boundingBox.x + boundingBox.width, boundingBox.y, 0f);
		Vector3 topRightAsViewport = camera.WorldToViewportPoint (topRight);
		
		if (topRightAsViewport.x >= topRightAsViewport.y)
		{
			orthographicSize = Mathf.Abs (boundingBox.width) / camera.aspect / 2f;
		}
		else
		{
			orthographicSize = Mathf.Abs (boundingBox.height) / 2f;
		}
		
		return Mathf.Clamp (Mathf.Lerp (camera.orthographicSize, orthographicSize, Time.deltaTime * zoomSpeed), minSize, Mathf.Infinity);
		//return Mathf.Clamp (orthographicSize, minSize, Mathf.Infinity);
	}
}
