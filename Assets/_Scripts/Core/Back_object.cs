﻿using UnityEngine;
using System.Collections;

public class Back_object : MonoBehaviour {

    private float Xmove_Speed;
    private float Xmove;
    private float Ymove;
    public bool Turn;
    public int Marzin;

    void Update()
    {
        Vector3 Scale = transform.localScale;

        Marzin++;

        if (Turn == false)
        {
            Xmove_Speed = -0.01f;
            Scale.y = 0.5f;
        }
        if (Turn == true)
        {
            Xmove_Speed = +0.01f;
            Scale.y = -0.5f;
        }

        if (Marzin > 1600)
        {
            if (Turn == false)
            {
                Turn = true;
                Marzin = 0;
            }

            else if (Turn == true)
            {
                Turn = false;
                Marzin = 0;
            }
        }




        Xmove = Xmove_Speed;
        transform.localScale = Scale;
        transform.Translate(0, Xmove, 0, Space.World);
    }
}
