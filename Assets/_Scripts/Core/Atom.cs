﻿using UnityEngine;

public enum AtomType {
	Normal, 
	Special
}

public class Atom 
{
	public int value = 0;
	public bool collected = false;

	// -- 
	private GameObject myGO;
	private Transform myTransform;

	private AtomType _AtomType;

	private float speed;


    //ケミカルX
	public Atom (int _value, Vector2 _position, float size, GameObject _a, AtomType _atomT)
	{
		// -- 
		myGO = new GameObject ("Atom_");
		myGO.AddComponent <SpriteRenderer> ().sprite = _a.GetComponent <SpriteRenderer> ().sprite;
        if(_atomT == AtomType.Special)
        {
            myGO.AddComponent<Animator>().runtimeAnimatorController = _a.GetComponent<Animator>().runtimeAnimatorController;
            var _animator = _a.GetComponent<Animator>();
        }

		myTransform = myGO.transform;

		if (!float.IsNaN (_position.x) && !float.IsNaN (_position.y))
		{
			// -- 
			myTransform.position = _position;
			myTransform.localScale = new Vector3 (size*1.3f, size*1.3f, 1);
			myTransform.SetParent (GameObject.Find ("_Atom Holder").transform);
			myTransform.Rotate (0, 0,  Random.Range (0, 360.0f));
			
			this._AtomType = _atomT;
			this.value = _value;
		}

		this.speed = Random.Range (-20.0f, 20.0f);
	}

	public void Update ()
	{
		if (myTransform == null)
			return;
		
		float rotation = speed * Time.deltaTime;
		myTransform.Rotate (0, 0, rotation);
	}

	// </3
	public void DestroyMe ()
	{
		GameObject.Destroy (this.myGO);
	}

	public AtomType GetAtomType ()
	{
		return this._AtomType;
	}

	public Vector2 GetPosition ()
	{
		return (this.myTransform == null) ? Vector2.zero : new Vector2 (this.myTransform.position.x, this.myTransform.position.y);
	}

	public Transform GetTransform ()
	{
		return this.myTransform;
	}

	public GameObject GetGameObject ()
	{
		return this.myGO;
	}
}
