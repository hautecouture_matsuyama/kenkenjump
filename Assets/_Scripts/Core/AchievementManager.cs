﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class Achievement
{
	public int reward = 100;
	public int valueToUnlock; 
	public int distanceA, distanceB;
	public bool isUnlocked; 
	public string Message;
}

public enum AchievementType
{
    GameCount,
	DeathCount,
    DeathBetween,
    CollectAtoms,
	CollectSpecialAtoms,
	Score,
    Start
};

public class AchievementEventArg : EventArgs
{
    public Achievement Data;
    public AchievementEventArg (Achievement e)
    {
        Data = e;
    }
}

public class AchievementsManager
{
    private Dictionary <AchievementType, List <Achievement>> _achievements;
    private Dictionary <AchievementType, int> _achievementKeeper;

    public event EventHandler <AchievementEventArg> AchievementUnlocked;

    protected virtual void RaiseAchievementUnlocked (Achievement ach)
    {
        var del = AchievementUnlocked as EventHandler <AchievementEventArg>;
        
		if (del != null)
        {
            del (this, new AchievementEventArg (ach));
        }
    }

    public AchievementsManager ()
	{
		_achievementKeeper = new Dictionary <AchievementType, int> ();

		_achievementKeeper.Add (AchievementType.CollectAtoms, 0);
		_achievementKeeper.Add (AchievementType.CollectSpecialAtoms, 0);
		_achievementKeeper.Add (AchievementType.DeathBetween, 0);
		_achievementKeeper.Add (AchievementType.DeathCount, 0);
		_achievementKeeper.Add (AchievementType.GameCount, 0);
	}

	public void RegisterDeathBetween (Achievement [] _active)
	{
		_achievementKeeper [AchievementType.DeathBetween] ++;
		ParseAchievements (AchievementType.DeathBetween, _active);
	}

	public void RegisterEvent (AchievementType type, Achievement[] active)
	{
		if(!_achievementKeeper.ContainsKey (type))
			return;
		
		switch(type)
		{
		case AchievementType.CollectAtoms:
			_achievementKeeper [type]++;
			break;
		case AchievementType.CollectSpecialAtoms:
			_achievementKeeper [type]++;
			break;
		case AchievementType.DeathCount:
			_achievementKeeper [type]++;
			break;
		case AchievementType.GameCount:
			_achievementKeeper [type]++;
			break;
		}

		ParseAchievements (type, active);
	}

	public void RegisterEvent (AchievementType type, Achievement[] _active, int amount)
	{
		if (!_achievementKeeper.ContainsKey (type))
		{
			return;
		}

		_achievementKeeper [type] += amount;
		
		ParseAchievements (type, _active);
	}

	/// <summary>
	/// Parses the achievements.
	/// </summary>
	public void ParseAchievements (AchievementType type, Achievement[] activeAchievements)
	{
		foreach (var kvp in _achievements.Where (a => a.Key == type))
		{
			foreach (var ach in kvp.Value.Where (a => a.isUnlocked == false))
			{
				if (_achievementKeeper [type] == ach.valueToUnlock)
				{
					//if (AchivementContains (ach, activeAchievements))
					//{
						ach.isUnlocked = true;
						RaiseAchievementUnlocked (ach);
					
						Reset (type);
					//}
				}
			}
		}
	}

	private bool AchivementContains (Achievement _ach, Achievement[] array)
	{
		for (int i = 0; i < array.Length; i++)
		{
			if (array [i] == _ach)
			{
				return true;
			}
		}

		return false;
	}

	public void Reset (AchievementType type)
	{
		if (!_achievementKeeper.ContainsKey (type))
		{
			return;
		}

		_achievementKeeper [type] = 0;
	}

	/// <summary>
	/// Gets all the completed achievements.
	/// </summary>
	public Achievement[] GetCompletedAchievements ()
	{
		List <Achievement> tmp = new List <Achievement> ();

		foreach (var kvp in _achievements)
		{
			foreach (var ach in kvp.Value.Where (a => a.isUnlocked == true))
			{
				tmp.Add (ach);
			}
		}

		return tmp.ToArray ();
	}

	public Achievement RandomAchievement (params Achievement[] others)
	{
		// -- 
		Achievement[] _achievements = this.GetAllAchievements ();
		
		// --
		Achievement _Achievement = _achievements [UnityEngine.Random.Range (0, _achievements.Length - 1)];
		
		foreach (Achievement _a in others)
		{
			if (_Achievement == _a)
			{
				_Achievement = RandomAchievement ();
			}
		}
		
		// -- 
		if (_Achievement.isUnlocked)
		{
			_Achievement = RandomAchievement ();
		}
		
		return _Achievement;
	}

	public Achievement RandomAchievement ()
	{
		// -- 
		Achievement[] _achievements = this.GetAllAchievements ();
		
		// --
		Achievement _Achievement = _achievements [UnityEngine.Random.Range (0, _achievements.Length - 1)];
		
		return _Achievement;
	}

	public Achievement[] GetAllAchievements ()
	{
		List <Achievement> tmp = new List <Achievement> ();
		
		foreach (var kvp in _achievements)
		{
			foreach (var ach in kvp.Value)
			{
				tmp.Add (ach);
			}
		}
		
		return tmp.ToArray ();
	}

	public void SaveAchievements ()
	{
		List <AchievementType> achievementType = new List <AchievementType> ();
		List <List <Achievement>> achievementData = new List <List <Achievement>> ();

		// -- Split Dictionary into 2 lists
		foreach (var entry in this._achievements)
		{
			achievementType.Add (entry.Key);
			achievementData.Add (entry.Value);
		}   

		// save them as 2 seperate lists
		PlayerPrefs.SetString ("AchievementType_Data", SaveUtils.ObjectToStr  <List <AchievementType>> (achievementType));
		PlayerPrefs.SetString ("Achievements_Data",    SaveUtils.ObjectToStr  <List <List <Achievement>>>     (achievementData));
	}

	public void LoadAchievements ()
	{
		if (LoadAchievementsData ().Count <= 0)
		{
			Debug.Log ("No Data to load, let's create some ..");

			_achievements = new Dictionary <AchievementType, List <Achievement>> ();

			_achievements.Add (AchievementType.CollectAtoms, AchievementData.AtomsAchievments);
			_achievements.Add (AchievementType.CollectSpecialAtoms, AchievementData.SpecialAtomsAchievments);
			_achievements.Add (AchievementType.GameCount, AchievementData.GameAchievments);
			_achievements.Add (AchievementType.DeathCount, AchievementData.DeathAchievments);
		}
		else
		{
			_achievements = LoadAchievementsData ();
		}
	}

	private Dictionary <AchievementType, List <Achievement>> LoadAchievementsData ()
	{
		Dictionary <AchievementType, List <Achievement>> tmp = new Dictionary <AchievementType, List <Achievement>> ();

		List <AchievementType> loadedAchievementType = SaveUtils.StrToObject <List <AchievementType>> (PlayerPrefs.GetString ("AchievementType_Data")); 
		List <List <Achievement>> loadedAchievementData = SaveUtils.StrToObject <List <List <Achievement>>> (PlayerPrefs.GetString ("Achievements_Data")); 

		if (loadedAchievementType == null || loadedAchievementData == null)
		{
			return tmp;
		}

		for (int i = 0; i < loadedAchievementData.Count; i++)
		{
			tmp.Add (loadedAchievementType [i], loadedAchievementData [i]);
		}

		return tmp;
	}
}

public static class AchievementData
{
	// how many <type> cogs have been collected
	public static List <Achievement> AtomsAchievments = new List <Achievement> ()
	{
		new Achievement () { valueToUnlock = 100,  isUnlocked = false, Message = "Collect 100 atoms in one game", reward = 500 },
		new Achievement () { valueToUnlock = 300,  isUnlocked = false, Message = "Collect 300 atoms in one game", reward = 1000 },
		new Achievement () { valueToUnlock = 500,  isUnlocked = false, Message = "Collect 500 atoms in one game", reward = 1500 },
		new Achievement () { valueToUnlock = 1000, isUnlocked = false, Message = "Collect 1000 atoms in one game", reward = 2000 },
		new Achievement () { valueToUnlock = 5000, isUnlocked = false, Message = "Collect a total of 5000 atoms", reward = 5000 }
	};

	public static List <Achievement> SpecialAtomsAchievments = new List <Achievement> ()
	{
		new Achievement () { valueToUnlock = 50,  isUnlocked = false,  Message = "Collect a total of 50 special atoms.", reward = 100 },
		new Achievement () { valueToUnlock = 100,  isUnlocked = false, Message = "Collect a total of 100 special atoms.", reward = 500 },
		new Achievement () { valueToUnlock = 300,  isUnlocked = false, Message = "Collect a total of 300 special atoms.", reward = 100 },
		new Achievement () { valueToUnlock = 500,  isUnlocked = false, Message = "Collect a total of 500 special atoms.", reward = 1500 },
		new Achievement () { valueToUnlock = 1000, isUnlocked = false, Message = "Collect a total of 1000 special atoms.", reward = 2000 }
	};


	// how many games played
	public static List <Achievement> GameAchievments = new List <Achievement> ()
	{
		new Achievement () { valueToUnlock = 10, isUnlocked = false, Message = "Play 10 games.", reward = 200 },
		new Achievement () { valueToUnlock = 30, isUnlocked = false, Message = "Play 30 games.", reward = 1000 },
		new Achievement () { valueToUnlock = 50, isUnlocked = false, Message = "Play 50 games.", reward = 3000 }
	};

	// how many times the player died
	public static List <Achievement> DeathAchievments = new List <Achievement> ()
	{
		new Achievement () { valueToUnlock = 3,  isUnlocked = false, Message = "Die 3 times.", reward = 100 },
		new Achievement () { valueToUnlock = 10, isUnlocked = false, Message = "Die 10 times.", reward = 500 }
	};



	public static List <Achievement> DistanceAchievments = new List <Achievement> ()
	{
		new Achievement () { distanceA = 1000, isUnlocked = false, Message = "Reach 100 meters.", reward = 100 },
		new Achievement () { distanceA = 300,  isUnlocked = false, Message = "Reach 300 meters.", reward = 300 },
		new Achievement () { distanceA = 500,  isUnlocked = false, Message = "Reach 500 meters.", reward = 1000 }
	};
}