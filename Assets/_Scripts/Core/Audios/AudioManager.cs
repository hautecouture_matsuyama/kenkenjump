﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    private const string BGM_VOLUME_KEY = "BGM_VOLUME_KEY";
    private const string SE_VOLUME_KEY = "SE_VOLUME_KEY";
    private const float BGM_VOLUME_DEFAULT = 1.0f;
    private const float SE_VOLUME_DEFAULT = 1.0f;

    public const float BGM_FADE_SPEED_RATE_HIGH = 0.9f;
    public const float BGM_FADE_SPEED_RATE_LOW = 0.3f;
    private float bgmFadeSpeedRate = BGM_FADE_SPEED_RATE_HIGH;



    #region Singleton

    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = (AudioManager)FindObjectOfType(typeof(AudioManager));
            }

            return instance;
        }
    }

    #endregion

    //次に再生するBGM、SE名
    private string nextBGMName;
    private string nextSEName;

    //BGMをフェードアウト中か
    private bool isFadeOut = false;

    public AudioSource BGMSource, SESource;

    private Dictionary<string, AudioClip> bgmDictionary, seDictionary; 

    public void Awake()
    {
        if(this != Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

        bgmDictionary = new Dictionary<string, AudioClip>();
        seDictionary = new Dictionary<string, AudioClip>();

        object[] bgmList = Resources.LoadAll("Audio/BGM");
        object[] seList = Resources.LoadAll("Audio/SE");

        foreach(AudioClip bgm in bgmList)
        {
            bgmDictionary[bgm.name] = bgm;
        }
        foreach(AudioClip se in seList)
        {
            seDictionary[se.name] = se;
        }
    }

    private void Start()
    {
        BGMSource.volume = PlayerPrefs.GetFloat(BGM_VOLUME_KEY, BGM_VOLUME_DEFAULT);
        SESource.volume = PlayerPrefs.GetFloat(SE_VOLUME_KEY, SE_VOLUME_DEFAULT);
    }

    public void PlaySE(string seName, float delay = 0.0f)
    {
        if (!seDictionary.ContainsKey(seName)){
            Debug.LogError(seName + "という名前のSEが見つかりませんでした。");
            return;
        }

        nextSEName = seName;
        Invoke("DelayPlaySE", delay);
    }

    private void DelayPlaySE()
    {
        SESource.PlayOneShot(seDictionary[nextSEName] as AudioClip);
    }

    public void PlayBGM(string bgmName, float fadeSpeedRate = BGM_FADE_SPEED_RATE_HIGH)
    {
        if (!bgmDictionary.ContainsKey(bgmName))
        {
            Debug.Log(bgmName + "という名前のBGMが見つかりませんでした。");
            return;
        }

        if (!BGMSource.isPlaying)
        {
            nextBGMName = "";
            BGMSource.clip = bgmDictionary[bgmName] as AudioClip;
            BGMSource.Play();
            Debug.Log("BGMは現在" + BGMSource.isPlaying);
        
        }
        else if (BGMSource.clip.name != bgmName)
        {
            nextBGMName = bgmName;
            FadeOutBGM(fadeSpeedRate);
        }
    }

    public void FadeOutBGM(float fadeSpeedRate = BGM_FADE_SPEED_RATE_LOW)
    {
        bgmFadeSpeedRate = fadeSpeedRate;
        isFadeOut = true;
    }

    private void Update()
    {
        if (!isFadeOut)
        {
            return;
        }

        BGMSource.volume -= Time.deltaTime * bgmFadeSpeedRate;
        if(BGMSource.volume <= 0)
        {
            BGMSource.Stop();
            BGMSource.volume = PlayerPrefs.GetFloat(BGM_VOLUME_KEY, BGM_VOLUME_DEFAULT);
            isFadeOut = false;

            if (!string.IsNullOrEmpty(nextBGMName))
            {
                PlayBGM(nextBGMName);
                Debug.Log("再生されるBGMは" + nextBGMName);
            }
        }
    }

}
