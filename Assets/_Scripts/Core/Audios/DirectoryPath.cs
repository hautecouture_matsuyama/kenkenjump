﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectoryPath{

    public const string TOP_RESOURCES = "Assets/Resources/";
    public const string AUDIO = "Audio";
    public const string BGM = AUDIO + "/BGM";
    public const string SE = AUDIO + "/SE";

    public const string AUTO_CREATING_CONSTANTS = "Assets/Scripts/Constants/AutoCreating/";
}
