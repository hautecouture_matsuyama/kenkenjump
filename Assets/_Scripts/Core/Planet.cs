﻿using UnityEngine;
using System.Collections;

// TODO : Ideally make a base class, then atoms / planets / fireballs etc extend from it.

[System.Serializable]
public class Planet
{
	private Vector2 position;
	private float size;
	private float speed, cachedSpeed;

	// -- 
	private GameObject myGO;
	private Transform myTransform;
	private bool clockwise = true;

    private Sprite mySprite;


	public bool currentPlanet;

	public Planet (Vector2 _position, float _size, float _speed, Sprite sprite, float _radius)
	{



		this.position = _position;
		this.size = _size;
		this.cachedSpeed = this.speed = _speed;

		// -- 
		myGO = new GameObject ("Planet_");
		myGO.AddComponent <SpriteRenderer> ().sprite = sprite;
		myGO.AddComponent <CircleCollider2D> ().radius = _radius;//(0.95f);

		// -- 
		myTransform = myGO.transform;
		myTransform.position = _position;
		myTransform.localScale = new Vector3 (this.size, this.size, 1);
	
		this.clockwise = (Random.value < 0.8f);
	}

	public Planet (Vector2 _position, float _size, float _speed, bool _cw, Sprite sprite, float _radius)
	{
		this.position = _position;
		this.size = _size;
		this.cachedSpeed = this.speed = _speed;
		
		// -- 
		myGO = new GameObject ("Planet_");
		myGO.AddComponent <SpriteRenderer> ().sprite = sprite;
		myGO.AddComponent <CircleCollider2D> ().radius = _radius;//(0.95f);

		// -- 
		myTransform = myGO.transform;
		myTransform.position = _position;
		myTransform.localScale = new Vector3 (this.size, this.size, 1);

		this.clockwise = _cw;
	}

	public void PauseRotation ()
	{
		this.speed = 0.0f;
	}

	public void ResumeRotation ()
	{
		this.speed = this.cachedSpeed;
	}

	public void Update ()
	{
		if (myTransform == null)
			return;

		float rotation = clockwise ? -speed * Time.deltaTime : speed * Time.deltaTime;
		myTransform.Rotate (0, 0, rotation);
	}

	public void DebugDraw ()
	{
		if (myTransform == null)
			return;

		Vector3 forward = myTransform.TransformDirection (Vector3.up) * 1.5f;
		Debug.DrawRay (myTransform.position, forward, Color.green);

		Gizmos.color = currentPlanet ? Color.green : Color.red;
		DebugExtension.DrawCircle (this.position, Vector3.back, Gizmos.color, this.size);
	}

	// </3
	public void DestroyMe ()
	{
		GameObject.Destroy (this.myGO);
	}

    public void SetLayer()
    {
       
        
    }

	public Vector2 GetPosition ()
	{
		return this.position;
	}

	public float GetSize ()
	{
		return this.size;
	}

	public Transform GetTransform ()
	{
		return this.myTransform;
	}

	public GameObject GetGameObject ()
	{
		return this.myGO;
	}
}
