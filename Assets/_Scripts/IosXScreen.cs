﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IosXScreen : MonoBehaviour {

	[SerializeField]
	private float PosX;

	void Awake(){
		if (Screen.width == 1125 && Screen.height == 2436) {
			ChangePosition ();
		}
	}

	private void ChangePosition(){
		Vector2 pos = GetComponent<RectTransform> ().anchoredPosition;
		pos.x = PosX;
		GetComponent<RectTransform> ().anchoredPosition = pos;
	
	}
}
