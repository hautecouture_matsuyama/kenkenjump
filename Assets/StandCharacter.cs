﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandCharacter : MonoBehaviour {


	[SerializeField]
	private Animator animator;

	private float defaultSpeed = 2.0f;

	public void SetAnimatorSpeed(){

		int level = PlayerPrefs.GetInt ("_AtomMultiplier");
		animator.speed = defaultSpeed * level;

		Debug.Log ("現在のマルチプルレベルは" + level);
	}
		
}
