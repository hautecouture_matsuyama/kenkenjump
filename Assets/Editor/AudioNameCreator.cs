﻿using System.IO;
using System.Text;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioNameCreator : AssetPostprocessorEx{

    private const string COMMAND_NAME = "Tools/Create/Audio Name";

    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        List<string[]> assetList = new List<string[]>()
        {
            importedAssets, deletedAssets, movedAssets, movedFromAssetPaths
        };

        List<string> targetDirectoryNameList = new List<string>()
        {
            DirectoryPath.TOP_RESOURCES + DirectoryPath.BGM,
            DirectoryPath.TOP_RESOURCES + DirectoryPath.SE
        };

        if(ExistsDirectoryInAssets(assetList, targetDirectoryNameList))
        {
            Create();
        }

    }

    [MenuItem(COMMAND_NAME)]
    private static void Create()
    {
        Dictionary<string, string> audioDic = Resources.LoadAll(DirectoryPath.AUDIO)
            .Select(audioFile => ((AudioClip)audioFile).name)
            .ToDictionary(audioFile => audioFile);
        ConstantsClassCreator.Create("AudioName", "オーディオ名を定数で管理するクラス", audioDic);
    }


}
