﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NendUnityPlugin.AD;

public class CloseButton : MonoBehaviour {

    public GameObject RankingCanvas;


    public void CloseRanking()
    {
        RankingCanvas.SetActive(false);

        if(RankingCanvas.gameObject.name != "RankingCanvas")
        Game.instance.ResumeGame();

    }

	public void Close(GameObject obj){
		obj.SetActive (false);
	}
}
