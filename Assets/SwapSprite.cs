﻿using UnityEngine;
using System.Collections;

public class SwapSprite : MonoBehaviour {

	public Sprite englishSprite;
	public Sprite chineseSprite;
	// Use this for initialization
	void Start () {
	
			GetComponent<SpriteRenderer>().sprite = englishSprite;
	
	}
	
	
}
